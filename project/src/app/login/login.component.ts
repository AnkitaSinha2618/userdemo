import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../appServices/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public login: any = {};
  public message: string = "";

  constructor(private service: AuthService, public router: Router) { }

  ngOnInit(): void {
  }

  public loginAction(): void {
    // let response = this.service.login(this.login);
    // if (response.status == "200") {
    //   localStorage.setItem("token", response.token);
    //   localStorage.setItem("role", response.role);
    //   let role = response.role;
    //   if (role == "developer") {
    //     this.router.navigate(['/users']);
    //   } else if (role == "teacher") {
    //     this.router.navigate(['/albums']);
    //   } else if (role = "salse") {
    //     this.router.navigate(['/posts']);
    //   }
    // } else {
    //   this.message = response.message;
    // }


    //login with server

    this.service.doLogin(this.login).subscribe((data: any) => {
      let token = data.token;
      if (token) {
        localStorage.setItem("token", token);
        const token_parts = token.split(".");
        let token_decoded = JSON.parse(window.atob(token_parts[1]));

        console.log(token_decoded);

        let role = token_decoded.ROLE[0];
        localStorage.setItem("role", role);
        
        console.log(role);
        
        if (role == "developer") {
          this.router.navigate(['/users']);
        } else if (role == "teacher") {
          this.router.navigate(['/albums']);
        } else if (role = "salse") {
          this.router.navigate(['/posts']);
        }

      } else {
        this.message = "Invalid Credentials";
      }

    });




  }

}
