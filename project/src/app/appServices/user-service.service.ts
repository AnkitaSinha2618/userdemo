import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }

  public url = "https://jsonplaceholder.typicode.com/";
  public url1 = "http://localhost:9090/api/";

  //User All User List
  public getUserList(): Observable<any> {
    return this.http.get(this.url + 'users')
  }

  //user user
  public getUser(id: number): Observable<any> {
    return this.http.get(this.url + 'users/' + id)
  }

  // user create
  public createUser(user: any): Observable<any> {
    return this.http.post(this.url + 'users', user);
  }

  //user update
  public updateUser(user: any, id: number): Observable<any> {
    user.id = id;
    return this.http.put(this.url + 'users/' + id, user)
  }

  //user Delete
  public deleteUser(id: number): Observable<any> {
    return this.http.delete(this.url + 'users/' + id)
  }


  ////////////////////////////////////////////

  //User All person List
  public getPersonList(): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem("token"));
    // let options = { headers: headers };
    //return this.http.get(this.url1 + 'person', options);

    return this.http.get(this.url1 + 'person');
  }

  //user user
  public getPerson(id: string): Observable<any> {
    // let headers = new HttpHeaders();
    // headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem("token"));
    // let options = { headers: headers };
    // return this.http.get(this.url1 + 'person/' + id, options);
    return this.http.get(this.url1 + 'person/' + id);
  }

  /*
    Interceptors are a unique type of Angular Service that we can implement.Interceptors 
    allow us to intercept incoming or outgoing HTTP requests using the HttpClient.
    By intercepting the HTTP request, we can modify or change the value of the request
    */


}
