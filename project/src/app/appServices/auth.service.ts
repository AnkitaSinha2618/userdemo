import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public url = "http://localhost:9090/user/login";

  //For Login
  public doLogin(login: any): Observable<any> {
    return this.http.post(this.url, login)
  }


  //For Login  
  // public login(login: any): any {
  //   if (login.email === "ranjeet" && login.password === "ranjeet") {
  //     let response: any = {};
  //     response.status = "200";
  //     response.token = "123";
  //     response.message = "success";
  //     response.role = "developer";
  //     return response;
  //   } else if (login.email === "ranjeet1" && login.password === "ranjeet1") {
  //     let response: any = {};
  //     response.status = "200";
  //     response.token = "123";
  //     response.message = "success";
  //     response.role = "teacher";
  //     return response;
  //   } else if (login.email === "ranjeet2" && login.password === "ranjeet2") {
  //     let response: any = {};
  //     response.status = "200";
  //     response.token = "123";
  //     response.message = "success";
  //     response.role = "sales";
  //     return response;
  //   } else {
  //     let response: any = {};
  //     response.status = "500";
  //     response.token = "null";
  //     response.message = "error";
  //     return response;
  //   }
  // }

}
